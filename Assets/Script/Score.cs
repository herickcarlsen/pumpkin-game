using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Score : MonoBehaviour
{
    public Collider2D ball;
    public Collider2D mouse;
    public static int scoreValue = 0; 
    private Text _score;

    private void Awake()
    {
        _score = GetComponent<Text>();
    }

    private void Update()
    {
 
        if (ball.IsTouching(mouse))
        {
            scoreValue += 1;
        }
        
        _score.text = "Score: " + scoreValue;
    }
}

