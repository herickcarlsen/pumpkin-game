using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class GameOver : MonoBehaviour
{

    public Collider2D ball;
    public Collider2D ground;
    public GameObject imageGameOver;
    
    // Update is called once per frame
    void Update()
    {

        if (ball.IsTouching(ground))
        {
            Time.timeScale = 0;
            imageGameOver.SetActive(true);

        }

        if (Input.GetButtonDown("Fire1"))
        {
            if (imageGameOver.activeSelf)
            {
                Time.timeScale = 1;
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
                Score.scoreValue = 0;
            }

        }


    }
}
