using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseFollow : MonoBehaviour
{

    private CircleCollider2D _cc;

    private void Awake()
    {
        _cc = GetComponent<CircleCollider2D>();
        _cc.enabled = false;

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            _cc.enabled = true;
        }


        if (Input.GetButtonUp("Fire1"))
        {
            _cc.enabled = false;
        }



        var position = (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.position = position;
    }
}
